import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReCorreoComponent } from './components/re-correo/re-correo.component';

const routes: Routes = [
  { path: 'Recorreo', component: ReCorreoComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'Recorreo'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
