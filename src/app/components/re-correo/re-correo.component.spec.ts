import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReCorreoComponent } from './re-correo.component';

describe('ReCorreoComponent', () => {
  let component: ReCorreoComponent;
  let fixture: ComponentFixture<ReCorreoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReCorreoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReCorreoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
