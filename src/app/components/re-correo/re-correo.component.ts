import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-re-correo',
  templateUrl: './re-correo.component.html',
  styleUrls: ['./re-correo.component.css']
})
export class ReCorreoComponent implements OnInit {
  forma!: FormGroup;

  constructor(private fb: FormBuilder) {
    this.crearCorreo();
   }

  ngOnInit(): void {
  }

  crearCorreo(): void{
    this.forma = this.fb.group({
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]
    ]
    });
  }
  Verificar(): void {
    console.log(this.forma.value);
    
  }
  get correoValido(){
    return this.forma.get('correo')?. valid &&  this.forma.get('correo')?.touched
  }

  get correoNoValido(){
    return this.forma.get('correo')?.invalid && this.forma.get('correo')?.touched;
  }

}
